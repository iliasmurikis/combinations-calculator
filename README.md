# Combinations Calculator

This project is useful for games like Words of Wonders or Crosswords, where you have specific letters in specific positions. With this script, you can see all the possible words and choose one that fits the position.
