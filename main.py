import itertools as it


def check_if_valid(combination):
    for character in list(set(combination)):
        if combination.count(character) != dict.get(character):
            return False
    return True


"""
    In dict you define what letters and how many you can use
                                                              """
dict = {'Ι': 2,
        'E': 1,
        'Δ': 1,
        'Π': 1,
        'Ρ': 1,
        'Κ': 1
        }
"""
    In structure you define what letters are applicable
    to every spot
                                                         """
structure = {'A': ['Ι', 'Δ', 'Π', 'Ρ', 'Κ'],
             'B': ['Ι', 'Δ', 'Π', 'Ρ', 'Κ'],
             'C': ['Ι', 'Δ', 'Π', 'Ρ', 'Κ'],
             'D': ['E'],
             'E': ['Δ', 'Π', 'Ρ', 'Κ'],
             'F': ['Ι']
             }

allNames = sorted(structure)
combinations = it.product(*(structure[Name] for Name in allNames))
# print(combinations)
for combination in combinations:
    if check_if_valid(combination):
        print(''.join(combination))
